/**
 * Use this modules to create a hyperlink like expirience in your cli applications.
 * This library basically creates an url shortener and uses very short ipv6 loopback urls
 * which are clickable in most terminals:
 * http://::1/this-is-an-example-link
 * 
 * To make this work, the module hosts a http server in the background, that redirects
 * the generated links to their original destinations.
 * 
 * This also limits the hyperlink functionality to the runtime of the cli app. After
 * closing the application, the hyperlinks will be useless. 
 */

import hyperlinkProxyServer from "./hyperlink-proxy-server";
import hyperlinkFactory, { CliHyperlink } from "./hyperlink-factory";
import chalk from 'chalk';
import * as rl from 'readline';

interface Options {
  interceptSIGINT: boolean
}

const DEFAULT_OPTIONS: Options = {
  interceptSIGINT: true
}

/**
 * Links defined with the `a` function will only be rendered as hyperlinks,
 * when `enable` function has been called.
 * Otherwise the links will be rendered like a markdown link: [http://example.com](Example)
 * The method is async, because a http server will be hosted to redirect the rewritten urls.
 */
export async function enableHyperlinks(options = DEFAULT_OPTIONS): Promise<void> {
  
  await hyperlinkProxyServer.listen();

  if(options.interceptSIGINT) {
    process.on("SIGINT", handleSigInt);
  }
}

let handlingSigInt = false;

/**
 * Because the hyperlinks in the console only work while the cli app is running,
 * we display a warning on SIGINT
 */
function handleSigInt() {
  if(handlingSigInt) return true;
  handlingSigInt = true;
  
  process.stdout.write(chalk.yellow.bold(">> Hyperlinks may not longer be working after this program has quit <<\n"));
  
  const cli = rl.createInterface(process.stdin, process.stdout);

  cli.question("Exit? [Y/n]", answer => {
    (answer || "Y").toUpperCase() === "Y" && process.exit(0)
    cli.close();
    handlingSigInt = false;
  });
}

export function hyperlink(url: string, text: string) {
  return hyperlinkFactory.create(url, text);
}


/**
 * Alias for the `hyperlink` function
 */
export function a(url: string, text: string): CliHyperlink {
  return hyperlink(url, text);
}