import express = require("express");
import getPort = require("get-port");
import hyperlinkFactory from "./hyperlink-factory";
import { range } from "lodash";

/**
 * Resolves the CliHyperlinks by forwarding to the real destination urls,
 * that were masked by the CliHyperlinks
 */
export class HyperlinkProxyServer {

  private isListening: boolean = false;
  private _port: number = 80;
  private server = express();

  constructor() {
    this.server.use((req, res, next) => {
      const redirectUrl = hyperlinkFactory.resolve(req.path);
      if (!redirectUrl) {
        return res.write(`hyperlink '${redirectUrl}' cannot be resolved`);
      }
      res.setHeader("Cache-Control", ["public", "max-age=3600"]);
      res.redirect(redirectUrl);
    });
  }

  get listening() {
    return this.isListening;
  }

  get port() {
    return this._port;
  }

  async listen() {
    if (this.isListening) {
      return;
    }
    this._port = await getPort({port: [80, 6, 8, 10, 12, 14, 16, 26, ...range(8000, 10000)]});

    await new Promise(async _ => this.server.listen(this._port, _));
    this.isListening = true;
  }
}

export default new HyperlinkProxyServer();