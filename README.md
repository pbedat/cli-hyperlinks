# cli-hyperlinks

Creates hyperlink like URLs in your CLI

## Usage
```
import * as hyperlinks from 'cli-hyperlinks';

async function main() {
  await hyperlinks.enableHyperlinks();

  console.log(`Learn more about ${hyperlinks.a("https://de.wikipedia.org/wiki/Hyperlink", "Hyperlinks")}!`);
}
```

As long as the application is running you can use the hyperlinks rendered by the cli application:

<img src="https://gitlab.com/pbedat/cli-hyperlinks/raw/master/docs/assets/example1.png">

## Api

### enableHyperlinks

`enableHyperLinks: () => Promise`

Enables the rendering of hyperlinks and launches a proxy that forwards the hyperlinks
to the original urls.

Note that this also keeps the cli open!

### hyperlink (Alias: a)

`hyperlink(url: string, text: string): Hyperlink`

Renders a hyperlink, that can be used in your cli:

`console.log(hyperlink("http://exmaple.com", "Link"))`

Creates an object with a .toString method that prints an url, pointing to a proxy,
which redirects to the original hyperlinked url.