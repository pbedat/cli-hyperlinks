import { enableHyperlinks, a } from "../src";

test();

async function test() {
  await enableHyperlinks();

  console.log(`Learn more about ${a("https://de.wikipedia.org/wiki/Hyperlink", "Hyperlinks")}!`);
}