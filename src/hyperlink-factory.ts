import hyperlinkProxyServer, { HyperlinkProxyServer } from "./hyperlink-proxy-server";
import chalk from "chalk";

/**
 * Creates and registers CliHyperlinks
 */
class HyperlinkRegistry {

  private pathUrlMap: Record<string, string> = {};

  /**
   * creates a cli hyperlink from the provded url and text
   */
  create(url: string, text: string) {
    let path = "";
    
    // try to generate a cli hyperlink path from the given text
    // avoids collisions by appending a counter prefix
    for (let i = 0; !path || this.pathUrlMap[path]; i++) {
      path = this.generateUrlPath(text) + (i === 0 ? "" : `_${i}`);
    }

    // map the generated path to the original url
    this.pathUrlMap[path] = url;
  
    return new CliHyperlink(path, url, text, hyperlinkProxyServer);
  }
  
  private generateUrlPath(text: string) {
    return "/" + encodeURI(text.replace(/\s/g, "_").replace(".", "").replace("/", "-"));
  }

  /**
   * Takes the path of a CliHyperlink and tries to resolve it to the
   * originally registered url.
   * @returns undefined when the path cannot be resolved
   * @param hyperlinkPath 
   */
  resolve(hyperlinkPath: string) {
    return this.pathUrlMap[hyperlinkPath];
  }
}

  /**
   * Used to defer the rendering of the url via .toString as long as possible.
   * We make this because, we cannot be sure if the application waits until
   * `enable()` is resolved
   */
  export class CliHyperlink {
    constructor(
      private urlPath: string,
      private url: string,
      private text: string,
      private proxy: HyperlinkProxyServer
    ) { }
    toString() {
      
      const { port, listening } = this.proxy;
      
      if(!listening) {
        return `[${this.url}](${this.text})`
      }
      
      const portSuffix = port === 80 ? "" : `:${port}`;
      return chalk.underline.blue(`http://[::1]${portSuffix}${this.urlPath}?`);
    }
  }

export default new HyperlinkRegistry();